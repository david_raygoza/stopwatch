import React, {Component} from 'react';
import {ScrollView, FlatList, StyleSheet, Text} from 'react-native';
import { render } from 'react-dom';
let padToTwo = (number) => (number <= 9 ? `0${number}`: number);
const styles = StyleSheet.create({
    scroll:{
        margin:5,
        maxHeight:"80%",
        backgroundColor: "#6BA8FF", 
        borderWidth:1,
        borderColor: "#0c0c0c",
        padding: 5,
    },
    item: {
        padding: 5,
        fontSize: 22,
        height: 35,
        color: "#5C415D",
        textAlign: "center",
        backgroundColor: "#fff",       
        marginTop:2,
        marginBottom:2,
        marginLeft:4,
        marginRight:4
    }
})
class ListComponent extends Component {
    render(){
        return (
            <ScrollView style={styles.scroll}>
                <FlatList 
                    data={this.props.lap}
                    keyExtractor = {(item, index) => index.toString()}
                    renderItem={({item, index}) => <Text key={index+1} style={styles.item}>{`#${index}            `}{padToTwo(item.min)}:{padToTwo(item.sec)}:{padToTwo(item.msec)}</Text>}                
                />
            </ScrollView>
        );
    }
}
export default ListComponent;